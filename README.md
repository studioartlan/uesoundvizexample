UESoundVizExample

1) Download the this example project:

	https://bitbucket.org/studioartlan/uesoundvizexample/downloads/


2) Download the UESoundViz plugin:

	https://bitbucket.org/studioartlan/uesoundviz/downloads/

and put it inside the Plugins folder of the example project.


3) Download the eXiSoundVis and OSC plugins:

	https://github.com/eXifreXi/eXiSoundVis
	https://github.com/monsieurgustav/UE4-OSC

and put them inside the Plugins folder of the example project.


4) Download audio from here:

https://drive.google.com/a/studioartlan.com/file/d/0B_4MFGKtOGe9el8wN21MbWRHLTg/view?usp=sharing

and put the ogg files inside Content/Audio folder.


5) Open the example project and start the level.